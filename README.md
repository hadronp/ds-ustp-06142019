# README #

This README outlines the steps in setting up the Python virtual environment, installing dependencies 
and running the Jupyter notebooks.

### What is this repository for? ###

This contains the Jupyter notebooks, datasets used in the Data Science talk at USTP, 14 June 2019.

### How do I get set up? ###

* Install conda for your platform https://conda.io/miniconda.html
* clone this repository ```git clone https://hadronp@bitbucket.org/hadronp/ds-ustp-06142019.git```
* navigate to the ```ds-ustp-06142019``` directory
* in the CLI type : ```conda create --name datascience pip python=3.6```
* to activate the virtual environment type: ```source activate datascience```
* Install packages and dependencies: ```pip install -r requirements.txt```
* run the JupyterLab notebook:  ```jupyter lab```
* open the notebook in the browser explorer 

### References ###

#### For Pandas and Data Munging guide
https://bitbucket.org/hadronp/9th-pytsada-meet-ds-pandas/src/master/

This presentation is based on https://towardsdatascience.com/how-to-build-a-simple-recommender-system-in-python-375093c3fb7d
The cleaned datasets is courtesy of https://github.com/mwitiderrick/simple-recommender-

### Who do I talk to? ###

* Repo owner or admin https://www.facebook.com/aryan.limjap
* https://www.facebook.com/groups/itgpytsada/